﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Funerarias.Models;

namespace Funerarias.Controllers
{
    public class ConsultaCliente1Controller : Controller
    {
        // GET: ConsultaCliente1
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetList()
        {
            List<Cliente> consulta = new List<Cliente>(); 
            using (FunerariasContext db = new FunerariasContext())
            {
                consulta = db.Clientes.ToList<Cliente>();
                return Json(new { data = consulta }, JsonRequestBehavior.AllowGet);

            }

    }
    }
}