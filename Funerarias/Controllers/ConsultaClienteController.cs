﻿using Funerarias.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Funerarias.Controllers
{
    public class ConsultaClienteController : Controller
    {
        FunerariasContext db = new FunerariasContext();
        // GET: ConsultaCliente
        public ActionResult Index()
        {
            return View(db.Clientes.ToList());
        }

        public JsonResult GetSearchingData(string buscarpor,string buscarValue)
        {
            List<Cliente> clientes1 = new List<Cliente>();
            if (buscarpor == "Id")
            {
                try
                {
                    int Id = Convert.ToInt32(buscarValue);
                    clientes1 = db.Clientes.Where(x => x.ClienteId == Id || buscarValue == null).ToList();
                }
                catch (FormatException)
                {
                    Console.WriteLine("{0} esto no es un codigo", buscarValue);
                    
                }
                return Json(clientes1, JsonRequestBehavior.AllowGet);
            }
            else
            {
                clientes1 = db.Clientes.Where(x => x.Nombre.Contains(buscarValue) || buscarValue == null).ToList();
                return Json(clientes1, JsonRequestBehavior.AllowGet);
            }
        }
    }
}