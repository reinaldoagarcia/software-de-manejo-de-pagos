﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Funerarias.Models;
using PagedList;
using PagedList.Mvc;


namespace Funerarias.Controllers
{
    public class ClientesController : Controller
    {
        private FunerariasContext db = new FunerariasContext();


        public ActionResult ConsultaCliente(int? pagina,string NombreCliente = null)
        {
            int tamanopagina = 4;
            int numeropagina = pagina ?? 1;
            var cliente = new Object();

            if (!String.IsNullOrEmpty(NombreCliente))
            {
                cliente = db.Clientes
                            .Where(c => c.Nombre.ToUpper().Contains(NombreCliente.ToUpper()))
                            .OrderBy(c => c.Nombre)
                            .ToPagedList(numeropagina, tamanopagina);
            }

            else
            {
                cliente = db.Clientes.OrderBy(p => p.Nombre).ToPagedList(numeropagina, tamanopagina);
            }
            

            return View("Index",cliente);

            //var clientes = db.Clientes.Take(5).Include(c => c.sucursal);
            //return View(clientes.ToList());
        }

        // GET: Clientes
        public ActionResult Index(int? pagina )
        {
            int tamanopagina = 3;
            int numeropagina = pagina ?? 1;

            return View(db.Clientes.OrderBy(p => p.Fecha).ToPagedList(numeropagina,tamanopagina));

            //var clientes = db.Clientes.Take(5).Include(c => c.sucursal);
            //return View(clientes.ToList());
        }

        // GET: Clientes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Cliente cliente = db.Clientes.Find(id);
            if (cliente == null)
            {
                return HttpNotFound();
            }
            return View(cliente);
        }

        // GET: Clientes/Create
        public ActionResult Create()
        {
            ViewBag.SucursalId = new SelectList(db.Sucursals, "SucursalId", "Nombre");
            ViewBag.PlanId = new SelectList(db.Plans, "PlanId", "descripcion");
            return View();
        }

        // POST: Clientes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Cliente cliente)
        {
            if (ModelState.IsValid)
            {
                db.Clientes.Add(cliente);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.SucursalId = new SelectList(db.Sucursals, "SucursalId", "Nombre", cliente.SucursalId);
            return View(cliente);
        }

        // GET: Clientes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Cliente cliente = db.Clientes.Find(id);
            if (cliente == null)
            {
                return HttpNotFound();
            }
            ViewBag.SucursalId = new SelectList(db.Sucursals, "SucursalId", "Nombre", cliente.SucursalId);
            ViewBag.PlanId = new SelectList(db.Plans, "PlanId", "Descripcion", cliente.PlanId);
            return View(cliente);
        }

        // POST: Clientes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit( Cliente cliente)
        {
            if (ModelState.IsValid)
            {
                db.Entry(cliente).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.SucursalId = new SelectList(db.Sucursals, "SucursalId", "Nombre", cliente.SucursalId);
            ViewBag.PlanId = new SelectList(db.Plans, "PlanId", "Descripcion", cliente.PlanId);
            return View(cliente);
        }

        // GET: Clientes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Cliente cliente = db.Clientes.Find(id);
            if (cliente == null)
            {
                return HttpNotFound();
            }
            return View(cliente);
        }

        // POST: Clientes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Cliente cliente = db.Clientes.Find(id);
            db.Clientes.Remove(cliente);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        //public ActionResult busquedafiltro (string sucursalId)
        //{
        //    var sucursal = from s in db. select s;
        //    if (!String.IsNullOrEmpty(sucursalId))
        //    {
        //        sucursal = sucursal.Where(j => j.sucursalId.Contains(sucursalId));

        //    }


        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
