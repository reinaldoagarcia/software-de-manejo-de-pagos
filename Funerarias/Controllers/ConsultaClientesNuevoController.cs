﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Funerarias.Models;

namespace Funerarias.Controllers
{
    public class ConsultaClientesNuevoController : Controller
    {
        private FunerariasContext db = new FunerariasContext();

        // GET: ConsultaClientesNuevo
        public ActionResult Index()
        {
            var clientes = db.Clientes.Include(c => c.plan).Include(c => c.sucursal);
            return View(clientes.ToList());
        }

        // GET: ConsultaClientesNuevo/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Cliente cliente = db.Clientes.Find(id);
            if (cliente == null)
            {
                return HttpNotFound();
            }
            return View(cliente);
        }

        // GET: ConsultaClientesNuevo/Create
        public ActionResult Create()
        {
            ViewBag.PlanId = new SelectList(db.Plans, "PlanId", "descripcion");
            ViewBag.SucursalId = new SelectList(db.Sucursals, "SucursalId", "Nombre");
            return View();
        }

        // POST: ConsultaClientesNuevo/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ClienteId,SucursalId,PlanId,Nombre,Cedula,Direccion,Telefono,Fecha")] Cliente cliente)
        {
            if (ModelState.IsValid)
            {
                db.Clientes.Add(cliente);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.PlanId = new SelectList(db.Plans, "PlanId", "descripcion", cliente.PlanId);
            ViewBag.SucursalId = new SelectList(db.Sucursals, "SucursalId", "Nombre", cliente.SucursalId);
            return View(cliente);
        }

        // GET: ConsultaClientesNuevo/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Cliente cliente = db.Clientes.Find(id);
            if (cliente == null)
            {
                return HttpNotFound();
            }
            ViewBag.PlanId = new SelectList(db.Plans, "PlanId", "descripcion", cliente.PlanId);
            ViewBag.SucursalId = new SelectList(db.Sucursals, "SucursalId", "Nombre", cliente.SucursalId);
            return View(cliente);
        }

        // POST: ConsultaClientesNuevo/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ClienteId,SucursalId,PlanId,Nombre,Cedula,Direccion,Telefono,Fecha")] Cliente cliente)
        {
            if (ModelState.IsValid)
            {
                db.Entry(cliente).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.PlanId = new SelectList(db.Plans, "PlanId", "descripcion", cliente.PlanId);
            ViewBag.SucursalId = new SelectList(db.Sucursals, "SucursalId", "Nombre", cliente.SucursalId);
            return View(cliente);
        }

        // GET: ConsultaClientesNuevo/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Cliente cliente = db.Clientes.Find(id);
            if (cliente == null)
            {
                return HttpNotFound();
            }
            return View(cliente);
        }

        // POST: ConsultaClientesNuevo/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Cliente cliente = db.Clientes.Find(id);
            db.Clientes.Remove(cliente);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
