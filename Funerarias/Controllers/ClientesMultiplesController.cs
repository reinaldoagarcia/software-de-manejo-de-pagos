﻿using Funerarias.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Funerarias.Controllers
{
    
    public class ClientesMultiplesController : Controller
    {
        private FunerariasContext db = new FunerariasContext();
        // GET: ClientesMultiples
        public ActionResult Index()
        {
            return View(db.Clientes.ToList());
        }
        [HttpPost]
        public ActionResult LoadData()
        {
            //jQuery DataTables Param
            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            //Find paging info
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var length = Request.Form.GetValues("length").FirstOrDefault();
            //Find order columns info
            var sortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault()
                                    + "][nombre]").FirstOrDefault();
            var sortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();
            //find search columns info
            var nombre = Request.Form.GetValues("columns[2][search][value]").FirstOrDefault();
            var planes = Request.Form.GetValues("columns[1][search][value]").FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt16(start) : 0;
            int recordsTotal = 0;




            using (FunerariasContext db = new FunerariasContext())
            {
                // dc.Configuration.LazyLoadingEnabled = false; // if your table is relational, contain foreign key
                var v = (from a in db.Clientes select a);

                //SEARCHING...
                if (!string.IsNullOrEmpty(nombre))
                {
                    v = v.Where(a => a.Nombre.Contains(nombre));
                }
                if (!string.IsNullOrEmpty(planes))
                {
                    v = v.Where(a => a.plan.descripcion ==planes);
                }
                //SORTING...  (For sorting we need to add a reference System.Linq.Dynamic)
                if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDir)))
                {
                    //v = v.OrderBy(sortColumn + " " + sortColumnDir);
                }

                recordsTotal = v.Count();
                var data = v.Skip(skip).Take(pageSize).ToList();
                return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data },
                    JsonRequestBehavior.AllowGet);

            }
        }
    }
}