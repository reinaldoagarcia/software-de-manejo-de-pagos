﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Funerarias.Models
{
    public class FunerariasContext : DbContext
    {
        // You can add custom code to this file. Changes will not be overwritten.
        // 
        // If you want Entity Framework to drop and regenerate your database
        // automatically whenever you change your model schema, please use data migrations.
        // For more information refer to the documentation:
        // http://msdn.microsoft.com/en-us/data/jj591621.aspx
    
        public FunerariasContext() : base("name=FunerariasContext")
        {
        }

        public System.Data.Entity.DbSet<Funerarias.Models.Producto> Productoes { get; set; }

        public System.Data.Entity.DbSet<Funerarias.Models.Sucursal> Sucursals { get; set; }

        public System.Data.Entity.DbSet<Funerarias.Models.Cliente> Clientes { get; set; }

        public System.Data.Entity.DbSet<Funerarias.Models.Plan> Plans { get; set; }
    }
}
