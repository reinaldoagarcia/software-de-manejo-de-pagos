﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Funerarias.Models
{
    public class Producto
    {
        [Key]
        public int productoId { get; set; }
        public string descripcion { get; set; }
        public decimal precio { get; set; }
        public DateTime fecha { get; set; }
        public float cantidad { get; set; }
        public float stock { get; set; }
    }
}