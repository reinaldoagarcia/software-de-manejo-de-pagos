﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Funerarias.Models
{
    public class Plan
    {
        [Key]
       public int PlanId { get; set; }
        public string descripcion { get; set; }
        public decimal precio { get; set; }
        public virtual ICollection<Cliente> clientes { get; set; }
    }
}