﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Funerarias.Startup))]
namespace Funerarias
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
